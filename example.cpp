#include <engine.h>
#include <scene.h>
#include <gameObject.h>
#include <iostream>

class Player : public gameObject
{
	
};

class Scene1 : public Scene
{
	public:
		Player player;
		Scene1(const Engine& engine) : Scene(engine)
		{

		}	
		void StartScene() override
		{
			player.SetTransform(300, 400, 15, 19);
			player.surface = SDL_CreateRGBSurface(0, player.transform.w, player.transform.h, 32, 0, 0, 0, 0);
			SDL_FillRect(player.surface, NULL, SDL_MapRGB(player.surface->format, 255, 255, 255));
			AddObjectToScene(&player);
		}
};

int main()
{
	Engine engine("Maid Engine -> Window", 640, 480);
	Scene1 scene1(engine);
	engine.AddScene(&scene1);
	engine.Start();
	return 0;
}
