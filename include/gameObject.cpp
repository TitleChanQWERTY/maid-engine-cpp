#include <SDL2/SDL.h>
#include <engine.h>
#include <gameObject.h>

gameObject::gameObject()
{
	
}

void gameObject::SetTransform(const int posx, const int posy, const int w, const int h)
{
	transform.x = posx;
	transform.y = posy;
	transform.w = w;
	transform.h = h;
}

void gameObject::Draw(const Engine engine)
{
	SDL_Texture* texture = SDL_CreateTextureFromSurface(engine.renderer, surface);
	SDL_RenderCopy(engine.renderer, texture, NULL, &transform);
	SDL_DestroyTexture(texture);
}

void gameObject::Update()
{
	
}
