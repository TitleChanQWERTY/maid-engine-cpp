#ifndef ENGINE_H
#define ENGINE_H

#include <SDL2/SDL.h>
#include <iostream>
#include <vector>

class Scene;

class Engine
{
	private:
		int size[2];
		char title[99];
		SDL_Window* window;
		int FPS;
		int desiredDelta;
		int StartTime;
		int CurrentScene;
		void tick();
	public:
		Engine(const char name[], const int w, const int h);
		int Start();
		void AddScene(Scene* scene);
		void ChangeScene(const int sceneNumber);
		SDL_Renderer* renderer;
};

#endif