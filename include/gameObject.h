#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <SDL2/SDL.h>
#include <engine.h>

class gameObject
{
	public:
		SDL_Surface* surface;
		SDL_Rect transform;
		gameObject();
		void SetTransform(const int posx, const int posy, const int w, const int h);
		void Draw(const Engine engine);
		void Update();
		void Move();
		void SetPos();
};

#endif