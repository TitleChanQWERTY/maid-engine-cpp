#ifndef SCENE_H
#define SCENE_H

#include <SDL2/SDL.h>
#include <iostream>
#include <gameObject.h>
#include <engine.h>
#include <vector>

class Scene
{
	private:
		std::vector<gameObject*> objectList;
		Engine _engine;
	public:
		Scene(const Engine& engine);
		void AddObjectToScene(gameObject* object);
		virtual void StartScene();
		void DrawScene();
		void UpdateScene();
};

#endif