#include <SDL2/SDL.h>
#include <engine.h>
#include <gameObject.h>
#include <scene.h>

Scene::Scene(const Engine& engine) : _engine(engine)
{

}

void Scene::AddObjectToScene(gameObject* object)
{
	objectList.push_back(object);
}

void Scene::DrawScene()
{
	for (int i; i < objectList.size(); i++)
	{
		objectList[i]->Draw(_engine);
	}
}

void Scene::UpdateScene()
{
	for (int i; i < objectList.size(); i++)
	{
		objectList[i]->Update();
	}
}

void Scene::StartScene()
{

}
