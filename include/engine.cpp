#include <SDL2/SDL.h>
#include <scene.h>
#include <engine.h>
#include <vector>
#include <iostream>

std::vector<Scene*> sceneList;

Engine::Engine(const char name[], const int w, const int h)
{
	size[0] = w;
	size[1] = h;
	FPS = 60;
	desiredDelta = 1000 / FPS;
	strcpy(title, name);
	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, size[0], size[1], SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

void Engine::AddScene(Scene* scene)
{
	sceneList.push_back(scene);
}

void Engine::ChangeScene(const int sceneNumber)
{
	CurrentScene = sceneNumber;
	sceneList[sceneNumber]->StartScene();
}

void Engine::tick()
{
	int delta = SDL_GetTicks() - StartTime;
	if (delta > desiredDelta) SDL_Delay(delta - desiredDelta);
}

int Engine::Start()
{
	bool running = true;

	SDL_Event event;

	sceneList[CurrentScene]->StartScene();

	while (running)
	{
		StartTime = SDL_GetTicks();
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT) running = false;
		}
		SDL_RenderClear(renderer);
		sceneList[CurrentScene]->DrawScene();
		sceneList[CurrentScene]->UpdateScene();
		SDL_RenderPresent(renderer);
		tick();
	}
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
